//
//  EditViewController.swift
//  navigationTarget
//
//  Created by 성인기 on 2020/10/10.
//

import UIKit

class EditViewController: UIViewController {

    @IBOutlet var editLabel: UILabel!
    @IBOutlet var editviewTextField: UITextField!
    @IBOutlet var editImg: UIImageView!
    
    @IBOutlet var sw: UISwitch!
    
    var aaa = "Asdf"
    var main: Editdata?
    var originImg : UIImage? //원본사진
    var changeImg : UIImage? // 바뀐사진
    var ischange = false
    var islamp = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //editLabel.text = "수정뷰입니다"
        editLabel.text = aaa //메인에서 받은 값을 처리
        originImg = UIImage(named: "22.jpeg")
        changeImg = UIImage(named: "20.jpeg")
        editImg.image = changeImg //바뀌는 화면에는 changeImg 넣어주기
        
        //lightSW.isOn = isLamp
    }
    
    @IBAction func mainGo(_ sender: UIButton) {
        
        
//        main?.gogo(msg: "viewcontrollr에 gogo를 불렀나요")
   //     main?.gogo(msg: editviewTextField.text!) //main에 data 전달
       
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func switchMainGo(_ sender: UISwitch) {
        if sender.isOn {
            editImg.image = originImg
            
        }else {
            
        }
        
        
    }
   
        
        
    
    
    
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        print("수정화면에서 넘어간다" + segue.identifier!)
//    }


}
