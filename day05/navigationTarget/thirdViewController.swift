//
//  thirdViewController.swift
//  day05
//
//  Created by 성인기 on 2020/10/11.
//

import UIKit

class thirdViewController: UIViewController {
    
    var main: Editdata?
    var islamp = true
    @IBOutlet var lampSW: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.hidesBackButton = true
        
        lampSW.isOn = true
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func goBack(_ sender: UIBarButtonItem) {
        
        navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


}
