//
//  ViewController.swift
//  day05
//
//  Created by 성인기 on 2020/10/10.
//

import UIKit
import MapKit



class ViewController: UIViewController,CLLocationManagerDelegate {

    @IBOutlet var myMap: MKMapView!
    @IBOutlet var addr1: UILabel!
    @IBOutlet var addr2: UILabel!
    
    @IBOutlet var latitude: UITextField!
    @IBOutlet var longitude: UITextField!
    
    
    let myLoc = CLLocationManager() // 내 위치정보 받을 변수 ,CLLocationManager : 앱위치 이동 변경 ,중지 사용 개체
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        myLoc.delegate = self // 위치정보 위임 <- 현재 클래스
       
        //gps 및 위치 정보 사용 승인이 필요
        myLoc.startUpdatingLocation() //gps 정보받기 -> locationManager()호출 // startUpdatingLocation: 사용자의 현재 위치를 보고하는 업데이트 생성 시작
       
        
        //gps 및 위치 정보 사용 승인이 필요
        myLoc.requestWhenInUseAuthorization()
        myMap.showsUserLocation = true
        
    }

    func setPoint(
        title:String,
        subtitle:String,
        ploc: CLLocationCoordinate2D) { //CLLoactionCoordinate2D 위치와 관련된 위도 및 경도, WGS 84 기준 프레임을 사용하여 지정함.
        
        
        let pin = MKPointAnnotation() //MKPointAnnotation 맵에 포인트 점 찍기.
        
        pin.coordinate = ploc //coordinate : 위도 및 경도로 지정된 주석의 좌표점.
        pin.title = title
        pin.subtitle = subtitle
        //지도에 핀 추가
        myMap.addAnnotation(pin)
        
    }
    
    
    
    
    //지도 이동 및 확대
    func goMap(title:String,
               subtitle:String,
        _ lat: CLLocationDegrees, //위도 CLLocationDegrees : 도 단위로 지정된 위도 또는 경도 값.
               _ lon: CLLocationDegrees, //경도
               _ sp: Double              //배율
               ) {
    
        //let loc2 = CLLocationCoordinate2DMake(<#T##latitude: CLLocationDegrees##CLLocationDegrees#>, <#T##longitude: CLLocationDegrees##CLLocationDegrees#>)
        
        // loc는 좌표                     매개변수: 위도 ,매개변수:경도
        let loc = CLLocationCoordinate2DMake(lat, lon)
       
        // span는 배율                   매개변수: 위도 배율  매개변수: 경도배율
        let span = MKCoordinateSpan(latitudeDelta: sp, longitudeDelta: sp)
        
        
        //rg는 좌표배율             상수 loc은 좌표   상수 span은 배율
        let rg = MKCoordinateRegion(center: loc, span: span)
       
        //좌표 및 배율 ,  애니메이션 유무
       
        myMap.setRegion(rg, animated: true)
        setPoint(title:title,subtitle:subtitle,ploc: loc) // setpoint plac 행위는 loc 상수
        
        if title != "현재위치가 아니다" {
           // setPoint(title: "현재위치", subtitle: "부위치", ploc:  37.501902, 127.025297)
            
        }
        
    }
    
    @IBAction func segmentChange(_ sender: UISegmentedControl) {
        //print(sender.selectedSegmentIndex)
        
        if sender.selectedSegmentIndex == 0 { //현재위치
            myLoc.startUpdatingLocation() // 내위치 받아줘. 윛정보 업데이트
            
        }
        
       else if sender.selectedSegmentIndex == 1 { //더조은 강남점
        
        //      lat : 위도 멤버변수          //lon : 경도 멤버변수       sp: 배율 멤버변수
        //goMap(<#T##lat: CLLocationDegrees##CLLocationDegrees#>, <#T##lon: CLLocationDegrees##CLLocationDegrees#>, <#T##sp: Double##Double#>)
        goMap(
            title: "현재위치", subtitle: "부위치",
            37.501902, 127.025297, 0.02)
        addr1.text = "더조은 강남점"
        addr2.text = "서울 서초구 서초4동 강남대로 441"
            
        }
        
       else if sender.selectedSegmentIndex == 2 { //우리집
        goMap(title: "현재위치", subtitle: "부위치",
            37.618833, 126.826801, 0.02)
        
      
        self.addr1.text = "우리집"
        self.addr2.text = "경기도 고양시 덕양구 토당동 406-13"
            
        }
        
        
    }
    
    //
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let lastLoc = locations.last
                                    //지리좌표정보
        print("내위치 갱신",lastLoc?.coordinate)
        
        goMap(
            title: "현재위치", subtitle: "부위치",
            (lastLoc?.coordinate.latitude)!, (lastLoc?.coordinate.longitude)!, 0.02)
        
        //절대좌표 -->행정지명 변경
        CLGeocoder().reverseGeocodeLocation(lastLoc!,completionHandler: {place,Error in
            
            
            let pm = place!.first!
            //print(pm.country!)
            var addr2txt = pm.country!
           
            addr2txt += pm.locality!+" " //시도
            //addr2txt += pm.thoroughfare! + " " //동
            
            self.addr1.text = "우리집"
            self.addr2.text = addr2txt
        })
        myLoc.stopUpdatingLocation() // 좌표받기 중지해
    }
    
 
    
    @IBAction func moveAction(_ sender: UIButton) {
        let latTTT = latitude.text!
        let longTTT = longitude.text!
        
        goMap(title: "현재좌표",
              subtitle: "어디",
              Double(latTTT)!,
              Double(longTTT)!,
              0.02)
        addr1.text = latTTT
        addr2.text = longTTT
      
        
    }
    
}




