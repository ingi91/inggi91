//
//  ViewController.swift
//  Splash_tutorial
//
//  Created by 성인기 on 2020/11/29.
//


/*
 plist -> 씬컨피규레이션-> 어플리케이션씬룰-> 에서 스토리보드 메인 삭제하기.
 
 */
import UIKit

class ViewController: UIViewController {
    let ui = ViewControllerUI()
    
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initSubView()
    }

    //화면넣기(아래 이미지 정의를 활용해서)
    private func initSubView() {
        self.view.backgroundColor = .gray
        let mainBackgroundImageView = ui.mainBackgroundImageView()
        self.view.addSubview(mainBackgroundImageView)
        
        let topTitleStackView = ui.topTitleStackView()
        self.view.addSubview(topTitleStackView)
        
        let bottomButtonStackView = ui.bottomButtonStackView()
        self.view.addSubview(bottomButtonStackView)
        
        NSLayoutConstraint.activate([
            mainBackgroundImageView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            mainBackgroundImageView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            mainBackgroundImageView.topAnchor.constraint(equalTo: self.view.topAnchor),
            mainBackgroundImageView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            
            topTitleStackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor,constant: 30),
            topTitleStackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor,constant: -30),
            topTitleStackView.topAnchor.constraint(equalTo: self.view.topAnchor,constant: self.view.frame.height * 0.24),
            
            bottomButtonStackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor,constant: 30),
            bottomButtonStackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor,constant: -30),
            bottomButtonStackView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor,constant: -self.view.frame.height * 0.14)
        ])
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
}

//이미지 정의
struct ViewControllerUI {
    func mainBackgroundImageView() -> UIImageView {
        let imageview = UIImageView(image: UIImage(named: "FastTyping"))
        imageview.translatesAutoresizingMaskIntoConstraints = true
        imageview.contentMode = .scaleToFill
        return imageview
    }
    
    func topTitleStackView() -> UIStackView {
       
       let topTitleLabel = UILabel()
        //backpack

        topTitleLabel.font = UIFont.systemFont(ofSize: 46, weight: .bold)
        topTitleLabel.textColor = UIColor(255,213,0)
        topTitleLabel.textAlignment = .center
        topTitleLabel.text = "BackPack"
        
        //46 bold  255 213 0
        //Travel with people. make new friend
        let bottomTitleLabel = UILabel()
        bottomTitleLabel.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        bottomTitleLabel.textColor = UIColor.white
        bottomTitleLabel.textAlignment = .center
        bottomTitleLabel.text = "Travel with people. make new friend"
       
        let stackview = UIStackView(arrangedSubviews: [topTitleLabel,bottomTitleLabel])
        stackview.translatesAutoresizingMaskIntoConstraints = false
        stackview.spacing = 15
        stackview.axis = .vertical
        stackview.alignment = .fill

        return stackview
               
    }
    
    
    func bottomButtonStackView() -> UIStackView {
        
        
        //sign up
        //color -> reverce
        let signUpButton = self.bottomButton("Sign up",UIColor(57,90,255), .white)
        
        //log in
        //color -> reverce
        let loginButton = self.bottomButton("log in",.white,UIColor(57,90,255))
        
        
        let stackview = UIStackView(arrangedSubviews: [signUpButton,loginButton])
        stackview.translatesAutoresizingMaskIntoConstraints = false
        stackview.axis = .vertical
        stackview.spacing = 20
        return stackview
    }
    private func bottomButton(_ title: String,_ textColor: UIColor,_ backgroundColor:UIColor) -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = backgroundColor
        button.layer.cornerRadius = 23
        //setAttributedTitle: 지정된 상태에 사용할 스타일이 지정된 제목을 설정합니다.
        button.setAttributedTitle(NSAttributedString(string: title, attributes: [
            NSAttributedString.Key.foregroundColor: textColor,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20,weight: .bold)
        ]), for: .normal)
        
        
        NSLayoutConstraint.activate([
            button.heightAnchor.constraint(equalToConstant: 45)
            
        ])
        return button
    }
}
