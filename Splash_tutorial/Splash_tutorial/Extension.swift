//
//  Extension.swift
//  Splash_tutorial
//
//  Created by 성인기 on 2020/11/29.
//

import Foundation
import UIKit


extension UIColor {
    convenience init(_ r: CGFloat,_ g: CGFloat,_ b: CGFloat,_ a: CGFloat = 1.0) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: a)
    }
}
